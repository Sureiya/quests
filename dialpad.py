import argparse
import itertools

argparser = argparse.ArgumentParser()
argparser.add_argument('numbers', type=str)

class DialPad(object):
    MAPPING = (
        (' '),
        ('.', ',', '?'),
        ('a', 'b', 'c'),
        ('d', 'e', 'f'),
        ('g', 'h', 'i'),
        ('j', 'k', 'l'),
        ('m', 'n', 'o'),
        ('p', 'q', 'r', 's'),
        ('t', 'u', 'v'),
        ('w', 'x', 'y', 'z'), 
    )

    def get_combos(self, numbers):
        # Casting string numbers to list eg ['4','3','5',5','6']
        numbers = list(numbers)
        # Using list comprehension to turn list of numbers into list of tuples
        # with mapped characters
        mapped_numbers = [self.MAPPING[int(x)] for x in numbers]
        # Using list comprehension here, along with itertools.product, which takes
        # an iterable and performs itertools.combinations recursively
        combos = [mnum for mnum in itertools.product(*mapped_numbers)]
        return combos

if __name__ == "__main__":
    args = argparser.parse_args()
    if not args.numbers.isdigit():
        raise argparse.ArgumentTypeError()
    dp = DialPad()
    for combo in dp.get_combos(''.join(args.numbers)):
        print(''.join(combo))